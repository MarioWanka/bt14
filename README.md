1)        Seznamte se s problematikou spolehlivostních modelů.

2)        Navrhněte metodu pro konstrukci spolehlivostních modelů číslicových obvodů využívajících nespolehlivé hlídací obvody.

3)        Zvolenou metodu implementujte.

4)        Ověřte funkčnost implementované metody výpočtem spolehlivostních parametrů reálného obvodu.

5)        Analyzujte vliv spolehlivosti hlídacích obvodů na spolehlivost celého systému.

6)        Diskutujte dosažené výsledky a navrhněte další směry rozvoje práce.
