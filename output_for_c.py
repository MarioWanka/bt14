#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: output_for_c.py
# Popis: Sourob generuje výstupní soubor s reprezentací přechodové matice a ostatních proměnných
#        Tento výstup slouží jako vstup pro program v jazyce C
# Parametry: Všechny parametry jsou uvedeny v kódu jako konstanty
#            Jedná se o pouze testovací sript 
# Požadavky: Nainstalovanou aplikace 'graphviz' pro generování grafu
#            Nainstalován balíček 'NumPy'

import data_store
import numpy
import time
import os

class Output():
    def __init__(self, file_name):
        self.file_handler = file(file_name, 'w')

    def write_line(self, line):
        self.file_handler.write(str(line) + "\n")
        
    def write(self, text):
        self.file_handler.write(str(text))

    def close(self):
        self.file_handler.close()

# funkce nam generuje mozne dalsi stavy v zavilosti na jednotkach, ktere se jeste mohou porouchat
# rozbiji pouze ty jednotky, ktere nejsou imunni
def damage_state(state, imunity):
    result = []
    for key in state.keys():
        newstate = state.copy()
        if ( state[key] > 0 ): # jestli se jeste muze neco rozbit, zkusime to rozbit
            if key in imunity.keys(): # je jednotka v seznamu nerozbitnych?
                if imunity[key] >= state[key]: # kontrola poctu nerozbitnych jednotek
                    continue # jednotku nerozbijej
            newstate[key] = newstate[key] - 1 # jinak jednotku rozbijeme
        result.append(newstate)
    return result

# funkce pro prehledny vypis matice
# UNUSED
def print_matrix(matrix, precision):
    for i, row in enumerate(matrix):
        print i,
        for col in row:
            print repr(col).rjust(precision),
        print

# funkce overi, zda stav 'state' odpovida korektnimu stavu
def state_is_correct(state, array_correct):
    for correct_state in array_correct:
        is_correct = True
        for key in correct_state.keys():
            if state[key] < correct_state[key]:
                is_correct = False
        if is_correct:
            return True
    return False

# funkce generuje z pocatecniho nerozbiteho stavu vsechny mozne stavy
# funkce tedy vraci seznam, ktery reprezentuje mozny stavovy prostor s ohledem na imunitu (nerozbitnost) jednotek
def generate_statespace(data):
    start_state = data.count.copy()
    
    states_queue = [start_state] # zacneme se startovacim stavem    
    visited=[] # pomocne pole jiz vygenerovanych stavu
    
    while len(states_queue) > 0:
        current_state = states_queue.pop()
        new_states = damage_state(current_state, data.imunity)
        visited.append(current_state)
        for st in new_states:
            if not st in visited:
                states_queue.append(st)
    return visited

# funkce dopocitava pravdepodobnost na hlavni diagonale prechodove matice
# hodnoty na hlavni diagonale udavaji pravdepodobnost, ze zustave v atkualnim stavu
def normalize_matrix(matrix, order_n):
    for i in range(0, order_n):
        result = 1.0
        for st in range(0, order_n):
            result -= matrix[i][st]
        matrix[i][i] = result
    return matrix

# funkce generuje popisky stavu pro graficke znazorneni pomoci programu 'graphviz'
# jedna se o pomocnou funkci pri generovani 'pdf' obrazku s Markovskym modelem
def label(state):
    keys = state.keys()
    keys.sort()
    label = ""
    for key in keys:
        label += str(key[0])
        label += str(state[key])
    return "\"" + label + "\""

# funkce generuje popisky prechodu mezi stavy
# jedna se o pomocnou funkci pri generovani 'pdf' obrazku s Markovskym modelem        
def what_change(start_state, end_state, probability):
    keys = start_state.keys()
    keys.sort()
    change = ""
    for key in keys:
        count = start_state[key] - end_state[key]
        if count > 0: # jednotka se rozbila
            if not change == "":
                change = change + " + "
            if count > 1:
                change = change + str(count) + "*"
            change = change + "L" + key[0].lower()
        elif count < 0: # jednotka byla spravena
            if not change == "":
                change = change + " + "
            if count < -1:
                change = change + str(count * -1) + "*"
            change = change + "M" + key[0].lower()
        else:
            pass
    if change == "":
        return probability
    else:
        return change

# funkce vygeneruje *.dot soubor, ze ktereho nasledne vygeneruje pomoci programu 'graphviz' graf Markovskeho modelu
# pomoci souboru *.dot vygeneruje nasledny *.pdf soubor
# je vytvoren potrebny soubor 'model.dot'
# pro generovani je volan prikaz 'dot -Tpdf file_name.dot -ofile_name.pdf'
def draw_dot_graph(file_name, visited, matrix, all_correctness):
    dot_file = file(file_name+".dot",'w')
    dot_file.write("digraph G {\n")
    dot_file.write("rankdir=LR;\n")
    # generujeme stavy, korektni v kolecku, poruchove ve ctverecku
    for i, state in enumerate(visited):
        if i in all_correctness:
            dot_file.write(label(state) + "[shape = circle] ;\n")
        else:
            dot_file.write(label(state) + "[shape = square] ;\n")
    # generujeme prechody mezi stavy
    for i_row, row in enumerate(matrix):
        for i_col, col in enumerate(row):
            if not col == 0.0:
                start_state = visited[i_col]
                end_state = visited[i_row]
                change = what_change(start_state, end_state, col)
                dot_file.write(label(start_state) + " -> " + label(end_state) + " [label = \"" + str(change) + "\"];\n")
    dot_file.write("}\n")
    dot_file.close()
    if not os.system("dot -Tpdf "+file_name+".dot -o"+file_name+".pdf") == 0: # nastala chyba
        print "Error: Generating state-graph with 'graphviz' failed."

# funkce vraci seznam vsech stavu, ktere jsou funkcni
# odpovidaji tedy pravidlum definovanym v sekci 'correctness'
def do_all_correctnes(visited, correctness):
    all_correctness = []
    for state in visited:
        if state_is_correct(state, correctness):
            try:
                index = visited.index(state)
                all_correctness.append(index)
            except:
                pass
    return all_correctness

# funkce overi, zda aktualni stav 'state' obsahuje potrebne jednotky pro opravu
# vraci odpovidajici hodnotu typu boolean
def is_repairable(state, must_working):
    repairable = True
    for key in must_working.keys():
        if state[key] < must_working[key]:
            repairable = False
#    print state, repairable
    return repairable

# funkce z predaneho vektoru 'vector' vypocte pravdepodobnost bezporuchoveho provozu R(t) pro vsechny korektni stavy
def calculate_result(all_correctness, vector):
    final_result = 0.0
    for index in all_correctness:
        final_result += vector[0][index]
    return final_result

# funkce se pouziva pro testovani, zda ma smysl pokracovat ve vypoctu dale
# funkce vraci True, pokud chceme vypocet ukoncit
# jinak vraci hodnotu False
def terminate_computation(new_vector, vector):
    # otestujeme, jestli se nektery radek matice rovna radku z minuleho kroku
    for i_row, row in enumerate(new_vector):
        if row == vector[i_row]:
            return True
        # otestujeme, jestli se nektery vysledek jiz nerovna jednicce
        if row[0] >= 1.0:
            return True
    return False
    
#######################
# Hlavni funkce
#######################
if __name__ == "__main__":
    data = data_store.Data_store()
    data.parse_XML_file('data3.xml')
#    data.print_all()
    
    visited = generate_statespace(data)
        
    order_n = len(visited)
    matrix = numpy.zeros((order_n, order_n))
    vector = numpy.zeros((1, order_n))
    
    # budeme zacinat z pocatecniho plne funkcniho stavu
    vector[0][0] = 1.0
    
    # generujeme matici prechodu
    # TODO: treba predelat, aby byla spravne sekce opravy
    for num_state, state in enumerate(visited): # pres vsechny mozne stavy
        temp_state = state.copy()
        repairable = is_repairable(temp_state, data.repair_working) # otestujeme, zda je stav opravitelny
        print num_state, temp_state
        for name in temp_state.keys(): # zkousime postupne rozbijet nebo opravovat vsechny soucastky
            if repairable: # je stav opravitelny?
                temp_state[name] = temp_state[name] + 1 # zkusime opravit
                try:
                    index = visited.index(temp_state)
                    matrix[num_state][index] = data.mu[name]
                except:
                    pass # aktualni soucastky tohoto typu jsou vsechny v poradku
                temp_state[name] = temp_state[name] - 1 # vratime do puvodniho stavu
            # endif: je stav opravitelny?
            temp_state[name] = temp_state[name] - 1 # zkusime rozbit
            try:
                index = visited.index(temp_state)
                matrix[num_state][index] = data.lam[name]
            except:
                pass # aktualni soucastka se nemuze uz polamat, vsechny uz jsou polamany
            temp_state[name] = temp_state[name] + 1
    
    matrix = normalize_matrix(matrix, order_n)
    
    print matrix
    print vector
    
    # seznam indexu vsech stavu, ktere odpovidaji pravidlum ze sekce 'correctness'
    all_correctness = do_all_correctnes(visited, data.correctness)
    
    name_file = "model"
    draw_dot_graph(name_file, visited, matrix, all_correctness)
    
    milisec_in_one_year = long(365 * 24 * 60 * 60 * 1000) # zavorka: pocet milisekund v roce
    milisec = milisec_in_one_year * 1
    milisec = 1000*360*1000 #TODO: testovaci hodnota 100 hodin v milisekundach
    milisec = 1000 * 3600 * 3 #TODO
    
    number_of_samples = 100
    one_step = milisec/number_of_samples
    
    out_file = Output("../computation_data")
    out_file.write_line(str(milisec))
    out_file.write_line(str(one_step))
    
    out_file.write_line(str(len(all_correctness)))
    for correct in all_correctness:
        out_file.write(str(correct)+" ")
    out_file.write("\n")
    
    out_file.write_line(str(order_n))
    for i in range(len(vector[0])):
        out_file.write(str(vector[0][i])+" ")
    out_file.write("\n")
    
    for row in matrix:
        for item in row:
            out_file.write(str(item)+" ")
        out_file.write("\n")
    
    out_file.close()
    exit()
    
    out_file.write_line("1 1.0")
    
    i = 1
    pause_mark = 0
    while i < milisec:
        pause_mark += one_step
        while i < pause_mark:
            vector = numpy.dot(vector, matrix)
            i += 1
        print "Progress:", float(pause_mark)/float(milisec) * 100, "%"
        out_file.write_line(str(i)+" "+str(calculate_result(all_correctness, vector)))
    
    out_file.close()
    