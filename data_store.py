#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: data_store.py
# Popis: Soubor osahuje difinici třídy pro práci s daty
#        Třída obsahuje datového struktury pro uložení potřebných hodnot a funkce pro prácí s nimi
#        Třída také obsahuje funkce na zpracování vstupního souboru

import xml.etree.ElementTree as etree

class Data_store():
    def __init__(self):
        self.count = dict()            # directory: (str) name => (int) count
        self.lam = dict()              # directory: (str) name => (float) lambda
        self.mu = dict()               # directory: (str) name => (float) mu
        self.correctness = []          # list of dictionarys (str) name => (int) count
        self.repair_working = dict()   # dictionary (str) name => (int) count
        self.repair_rules = []         # list of tuples (priority, dict_of_component, list_of_priority)
        self.imunity = dict()          # dictionary: (str) name => (int) count
        self.samples = 100             # number of samples (default value = 100)
        self.time = 1000000            # end time of computation
  
    ##############################################################################################################
    # Pomocne funkce
    
    # Funkce testuje, zda jednotka s nazvem 'component' byla jiz drive definovana v 'Architecture'
    def is_defined(self, component):
        if component in self.count.keys():
            return True
        print "Component", component, "is not defined in architecture."
        return False

    ###############################################################################################################
    # Architecture
    
    # Funkce provadi typovou kontrolu hodnot, ktere predstavuji udaje o jednotce
    # Pokud je kontrola v poradku, ulozi hodnoty do patricnych slovniku pod klicem nazvu jednotky
    def add_architecture(self, (name, count, lam, mu)):
        try:
            name = str(name)
            count = int(count)
            lam = float(lam)
            mu = float(mu)
            self.count[name] = count
            self.lam[name] = lam
            self.mu[name] = mu
        except:
            print "Function 'add_architecture' expect tuple ((str) name, (int) count, (float) lambda, (float) mu)"
    
    # Funkce vraci slovnik predstavujici pocty jednotek v systemu
    def get_architecture(self):
        return self.count
    
    # Funkce vraci slovnik predstavujici hodnoty 'lambda' pro dane jednotky
    def get_lambda(self):
        return self.lam
    
    # Funkce vraci slovnik predstavujici hodnoty 'mu' pro dane jednotky
    def get_mu(self):
        return self.mu

    ###############################################################################################################
    # Correctness
    
    # Funkce provadi typovou kontrolu hodnot a provadi nasledne vlozeni korektniho stavu do seznamu vsech korektnich stavu
    # Pokud je kontrola v poradku, ulozi hodnoty do seznamu korektnich stavu
    # Vstupem funkce je pole dvojic (name, count), ktere prezentuje jednotky a jejich pocet, aby byl system provozuschopny
    def add_correctness(self, array):
        try:
            new_dict = dict()
            all_passed = True
            for item in array:
                name = str(item[0])
                count = int(item[1])
                if self.is_defined(name):
                    new_dict[name] = count
                else:
                    all_passed = False

            if all_passed:
                self.correctness.append(new_dict)
            else:
                raise
        except:
            print "Function 'add_correctness' expect list of tuples ((str) name, (int) count)"

    # Funkce vraci seznam vsech korektnich stavu, jak je nadefinoval uzivatel
    # Tento seznam je pro dalsi praci treba doplnit o vsechny korektni stavy
    def get_correctness(self):
        return self.correctness

    ###############################################################################################################
    # Repair Rules - Working
    
    # Funkce provadi typovou kontolu a uklada do slovniku jednotky, ktere musi byt funkci v systemu, aby mohla byt provadena oprava
    def add_repair_working(self, (name, count)):
        try:
            name = str(name)
            count = int(count)
            if self.is_defined(name):
                self.repair_working[name] = count
            else:
                raise
        except:
            print "Function 'add_repair_working' expect tuple ((str) name, (int) count)"
    
    # Funkce vraci jednotky, ktere jsou nutne pro provadeny opravy
    def get_repair_working(self):
        return self.repair_working
    
    ###############################################################################################################
    # Repair Rules - Priority rules
    
    # Vstupem funkce je trojice (priority, list_of_component, list_of_priority)
    # Polozka 'priority' udava prioritu daneho pravidla pro opravu
    # Polozka 'list_of_component' je seznam jednotek, ktere odpovidaji aktualnimu stavu jednotek
    # Posledni polozka 'list_of_priority' je seznam, ktery definuje dle priorit v jakem poradi budou jednotky opravovany
    def add_repair_rules(self, (priority, list_of_component, list_of_priority)):
        try:
            priority = int(priority)
            dict_component = dict()
            # check component list
            for item in list_of_component:
                name = str(item[0])
                count = int(item[1])
                if not self.is_defined(name):
                    raise
                dict_component[name] = count
            # check priority list
            new_priority_list = []
            for item in list_of_priority:
                name = str(item[0])
                priority = int(item[1])
                if not self.is_defined(name):
                    raise
                new_priority_list.append((name, priority))
            new_priority_list.sort(key=lambda item: item[1])
            new_priority_list.reverse()

            self.repair_rules.append( (priority, dict_component, new_priority_list) )
            self.repair_rules.sort()
            self.repair_rules.reverse()
        except:
            print "Function 'add_repair_rules' expect tuple ((int) priority, (list) component, (list) priority)"
            print "(list) component consist of tuples ((str) name, (int) count)"
            print "(list) priority consist of tuples ((str) name, (int) priority)"

    # Funkce vraci seznam trojic, ktere reprezentuji jednotliva prioritni pravidla pro opravu
    def get_repair_rules(self):
        return self.repair_rules

    ###############################################################################################################
    # Imunity
    
    # Funkce provadi kontrolu hodnot a prida jednotku se zadanym poctem do seznamu imunity
    def add_imunity(self, (name, count)):
      try:
          name = str(name)
          count = int(count)
          self.imunity[name] = count
      except:
          print "Function 'add_imunity' expect tuple ((str) name, (int) count)"

    # Funkce vraci seznam nerozbitnych jednotek
    def get_imunity(self):
        return self.imunity
    
    ###############################################################################################################
    # Computation
    
    # Funkce uklada potrebne promenne pro samotny vypocet
    def add_computation(self, (time, samples)):
      try:
          time = int(time)
          samples = int(samples)
          self.time = time
          self.samples = samples
      except:
          print "Function 'add_computation' expect tuple ((int) time, (int) samples)"

    # Funkce vraci promenne 'time' a 'samples' potrebne pro vypocet
    def get_computation(self):
        return (self.time, self.samples)

    ###############################################################################################################
    # Section for parse XML file
    
    # Funkce zpracovava sekci definice architektury systemu
    def parse_architecture (self, root):
        result = []
        for child in root:
            if child.tag == "architecture":
                for component in child:
                    name = ""
                    count = ""
                    lam = ""
                    mu = ""
                    if component.tag == "component":
                        name = component.attrib['name']
                        count = component.attrib['count']
                        for value in component:
                            if value.tag == "lambda":
                                lam = value.text
                            elif value.tag == "mu":
                                mu = value.text
                    if name and count and lam and mu:
                        result.append((name, count, lam, mu))
                    else:
                        print "There is an error in XML file in Architecture section."
        return result
    
    # Funkce zpracovava sekci korektnich stavu
    def parse_correctness(self, root):
        result = []
        for child in root:
            if child.tag == "correctness":
                for correct in child:
                    if correct.tag == "correct":
                        correctness = []
                        name = []
                        count = []
                        for component in correct:
                            if component.tag == "component":
                                name = component.attrib['name']
                                count = component.attrib['count']
                            if name and count:
                                correctness.append((name, count))
                            else:
                                print "There is an error in XML file in Correctness section."
                    if correctness:
                        result.append(correctness)
                    else:
                        print "There is an error in XML file in Correctness section."
        return result
    
    # Tato pomocna funkce zpracovava jedno pravidlo pro opravu
    def __parse_priorrules(self, priorrules):
        result = []
        for child in priorrules:
            priority = []
            components = []
            repair_rules = []
            if child.tag == "priorityRule":
                priority = child.attrib['value']
                name = []
                value = []
                for item in child:
                    if item.tag == "component":
                        name = item.attrib['name']
                        value = item.attrib['count']
                        if name and value:
                            components.append((name, value))
                    elif item.tag == "priorityVal":
                        name = item.attrib['name']
                        value = item.text
                        if name and value:
                            repair_rules.append((name, value))
                        else:
                            print "There is an error in XML file in Priority Rules section."
            if priority and repair_rules:
                result.append((priority, components, repair_rules))
            else:
                print "There is an error in XML file in Priority Rules section."
        return result
    
    # Funkce zpracovava postupne vsechna pravidla pro opravu
    def parse_repair_rules(self, root):
        working = []
        reprules = []
        for child in root:
            if child.tag == "repairRules":
                for item in child:
                    name = []
                    count = []
                    if item.tag == "component":
                        name = item.attrib['name']
                        count = item.attrib['count']
                        if name and count:
                            working.append((name, count))
                    elif item.tag == "priorRules":
                        reprules = self.__parse_priorrules(item)
                    else:
                        print "There is an error in XML file in Priority Rules section."
        
        return working, reprules
    
    # Funkce zpracovava sekci nerozbitnych jednotek
    def parse_imunity(self, root):
        result = []
        for child in root:
            if child.tag == "imunity":
                for component in child:
                    name = []
                    count = []
                    if component.tag == "component":
                        name = component.attrib['name']
                        count = component.attrib['count']
                    if name and count:
                        result.append((name, count))
        return result
    
    # Funkce zpracovava sekci s definici promennych nutnych pro vypocet
    def parse_computation(self, root):
        for child in root:
            if child.tag == "computation":
                time = []
                samples = []
                for computation in child:
                    if computation.tag == "time":
                        time = computation.text
                    if computation.tag == "samples":
                        samples = computation.text
        return (time, samples)
    
    # Funkce postupne nacita vsechny sekce ze vstupniho XML souboru
    def parse_XML_file(self, file_name):
        tree = etree.parse(file_name)
        root = tree.getroot()
        # Architecture
        for item in self.parse_architecture(root):
            self.add_architecture(item)
        # Correctness
        for item in self.parse_correctness(root):
            self.add_correctness(item)
        # Repair rules
        working, repair_rules = self.parse_repair_rules(root)
        for item in working:
            self.add_repair_working(item)
        for item in repair_rules:
            self.add_repair_rules(item)
        # Imunity    
        for item in self.parse_imunity(root):
            self.add_imunity(item)
        # Computation settings
        self.add_computation(self.parse_computation(root))
    
    # Funkce postupne vypisuje hodnoty vsech struktur a promennych, ktere jsou aktualne ulozeny v tride
    # Funkce je pouzivana predevsim pro testovani
    def print_all(self):
        print "Architecture"
        print "Count", data.count
        print "Lambda", data.lam
        print "Mu", data.mu
        print "Correctness"
        for item in self.get_correctness():
            print item
        print "Repair, must working"
        print self.repair_working
        print "Repair rules"
        for item in self.get_repair_rules():
            print item
        print "Imunity"
        print self.imunity
        print "Computation"
        print "Samples:", self.samples
        print "Time:", self.time

if __name__ == "__main__":
    # priklad pouziti
    data = Data_store()
    data.parse_XML_file('data3.xml')
    data.print_all()
    
