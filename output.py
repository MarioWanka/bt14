#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: output.py
# Popis: Soubor obsahuje definice porpůrné třídy pro práci s výstupním souborem

# trida pro zapis do vystupniho souboru
class Output():
    # pri konstrukci tridy je treba zadat jmeno vystupniho souboru
    def __init__(self, file_name):
        self.file_handler = file(file_name, 'w')

    # funkce zapise do souboru zadany retezec a prida odradkovani
    def write_line(self, line):
        self.file_handler.write(str(line) + "\n")
        
    # funkce zapise do souboru zadany retezec
    def write(self, text):
        self.file_handler.write(str(text))

    # funkce uzavre soubor a ukonci tak praci s nim
    def close(self):
        self.file_handler.close()