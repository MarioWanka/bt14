#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: dot_graph.py
# Popis: Soubor obsahuje definici třídy pro generování přechodového grafu
# Požadavky: Nainstalovanou aplikace 'graphviz' (je použit příkaz 'dot')


import os

# trida pro generovani orientovaneho grafu pomoci programu 'graphviz'
class Dot_graph():
    # pri konstrukci tridy je treba zadat jmeno vystupniho souboru
    def __init__(self, file_name):
        self.file_name = file_name
    
    # funkce generuje popisky stavu pro graficke znazorneni pomoci programu 'graphviz'
    # jedna se o pomocnou funkci pri generovani 'pdf' obrazku s Markovskym modelem
    def __label(self, state):
        keys = state.keys()
        keys.sort()
        label = ""
        for key in keys:
            label += str(key[0])
            label += str(state[key])
        return "\"" + label + "\""
    
    # funkce generuje popisky prechodu mezi stavy
    # jedna se o pomocnou funkci pri generovani 'pdf' obrazku s Markovskym modelem        
    def __what_change(self, start_state, end_state, probability):
        keys = start_state.keys()
        keys.sort()
        change = ""
        for key in keys:
            count = start_state[key] - end_state[key]
            if count > 0: # jednotka se rozbila
                if not change == "":
                    change = change + " + "
                if count > 1:
                    change = change + str(count) + "*"
                change = change + "L" + key[0].lower()
            elif count < 0: # jednotka byla spravena
                if not change == "":
                    change = change + " + "
                if count < -1:
                    change = change + str(count * -1) + "*"
                change = change + "M" + key[0].lower()
            else:
                pass
        if change == "":
            return probability
        else:
            return change
    
    # funkce vygeneruje *.dot soubor, ze ktereho nasledne vygeneruje pomoci programu 'graphviz' graf Markovskeho modelu
    # pomoci souboru *.dot vygeneruje nasledny *.pdf soubor
    # je vytvoren potrebny soubor 'model.dot'
    # pro generovani je volan prikaz 'dot -Tpdf file_name.dot -ofile_name.pdf'
    def draw(self, visited, matrix, all_correctness):
        dot_file = file(self.file_name+".dot",'w')
        dot_file.write("digraph G {\n")
        dot_file.write("rankdir=LR;\n")
        # generujeme stavy, korektni v kolecku, poruchove ve ctverecku
        for i, state in enumerate(visited):
            if i in all_correctness:
                dot_file.write(self.__label(state) + "[shape = circle] ;\n")
            else:
                dot_file.write(self.__label(state) + "[shape = square] ;\n")
        # generujeme prechody mezi stavy
        for i_row, row in enumerate(matrix):
            for i_col, col in enumerate(row):
                if not col == 0.0:
                    start_state = visited[i_row]
                    end_state = visited[i_col]
                    change = self.__what_change(start_state, end_state, col)
                    dot_file.write(self.__label(start_state) + " -> " + self.__label(end_state) + " [label = \"" + str(change) + "\"];\n")
        dot_file.write("}\n")
        dot_file.close()
        if not os.system("dot -Tpdf "+self.file_name+".dot -o"+self.file_name+".pdf") == 0: # nastala chyba
            print "Error: Generating state-graph with 'graphviz' failed."
          
