#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: exp.py
# Popis: Soubor pro testování a lepší pochopení exponencionálního rozdělení
#        Uveden pouze pro zajímavost a úplnost

from math import log
from random import expovariate, random

mid_value = 15
lam = float(1.0 / mid_value)
lam = 0.3
mine = []
their = []
samples = 1000
for i in range(1, samples):
    mine.append(-(1/lam) * log(random()))
    their.append(expovariate(lam))
    
mine.sort()
their.sort()

print mine[0], mine[samples/2], mine[-1]
print sum(mine)/samples
print their[0], their[samples/2], their[-1]
print sum(their)/samples
