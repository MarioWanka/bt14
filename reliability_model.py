#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: reliability_model.py
# Popis: Soubor načítá definici systému ze vstupního souboru XML
#        Sestaví markovský řetězec a podle zvolené metody jej řeší
#        Výstupem jsou tři soubory: 
#                       output.dot - definice přechodového grafu pro aplikaci 'graphviz' 
#                       output.pdf - samotný obrázek grafu (výstup aplikace
#                       output - soubor s vypočítaními výsledky
# Parametry: input.xml - vstupni soubor s definici systemu
#            output - nazev pro vystupni soubory
#            metoda - vybere se z mnoziny {ssa, numeric} pro pozadovanou metodu
#            iterations - je zadáván pouze při použití metody ssa
# Požadavky: Nainstalovanou aplikace 'graphviz' pro generování grafu
#            Nainstalován balíček 'NumPy'
# Použití: reliability_model.py input.xml output {ssa | numeric} (iterations)

# uzivatelkse knihovny
import data_store
import output
import dot_graph
from matrix import *

# vestavene a ostatni knihovny
import numpy    # dot, mean, median, zeros
import time     # sleep
import os       # os.system
import random   # random, seed, expovariate
import sys      # argv


#######################
# Hlavni funkce
#######################
if __name__ == "__main__":
    # Kontrola zadanych argumentu
    try:
        if len(sys.argv) < 4:
            raise

        INPUT = str(sys.argv[1])
        OUTPUT = str(sys.argv[2])
        METHOD = str(sys.argv[3])
        
        # je zvolena odpovidajici metoda?
        if not METHOD in ["ssa", "numeric"]:
            raise
        
        # pokud je zvolena simulace, je treba zadat pocet iteraci pro vypocet
        if METHOD == "ssa":
            if len(sys.argv) < 5:
                raise
            else:
                try:
                    # pocet iteraci ovlivnuje rychlost a hlavne vyslednou presnost vypoctu
                    ITERATIONS = int(sys.argv[4])
                except:
                    raise

    except:
        print "Wrong argument."
        print "USAGE: ./reliability_model.py input_file.xml output_file {ssa|numeric} (iterations)"
        print "Argument 'iterations' is given only with ssa method."
        exit(1)
    
    # nacteme vsechna data do datoveho uloziste
    data = data_store.Data_store()
    data.parse_XML_file(INPUT)
    #data.print_all()
    
    # vygenerujeme stavovy prostor, vypocteme rad matice
    visited = generate_statespace(data)
    order_n = len(visited)
    
    matrix = make_matrix(visited, order_n, data)
    #matrix = make_matrix_random_repair(visited, order_n, data)
    
    # seznam indexu vsech stavu, ktere odpovidaji pravidlum ze sekce 'correctness'
    all_correctness = do_all_correctnes(visited, data.correctness)
    
    # vygeneruje pomocny soubor s pripomonou dot a nasledny pdf soubor s orientovanym prechodovym grafem
    if order_n <= 128:
        dot_graph = dot_graph.Dot_graph(OUTPUT)
        dot_graph.draw(visited, matrix, all_correctness)
    else:
        print "Order of matrix is to big for ploting 'dot' graph."
    
    #for i,state in enumerate(visited):
    #    print i, state
    #print_matrix(matrix)
    #print matrix[1]
    #print order_n
    #exit()
    
    # Cas ukonceni vypoctu, pocet vzorku, casovy krok mezi vypisy vystupu
    TIME = data.time
    NUMBER_OF_SAMPLES = data.samples
    ONE_STEP = TIME/NUMBER_OF_SAMPLES
    
    # podle zvolene metody vypocteme model
    # Stochastic Simulation Algorithm
    if METHOD == "ssa":
        # predpocitame vektor hodnot lambda pro setrvani ve stavu
        lambda_vector = []
        lambda_vector = make_lambda_vector(matrix, order_n)
    
        # predchystani slovniku, do ktereho budeme ukladat vysledky
        results = dict()
        i = 0
        while i <= TIME:
            results[i] = []
            i += ONE_STEP
        results[0] = 1.0
    
        # samotna simulace Markovskych retezcu v danem poctu iteraci
        for iter in range(ITERATIONS):
            #print "Iteration:", iter+1, "/", ITERATIONS
            print "Progress:", float(iter+1)/float(ITERATIONS) * 100, "%"
            i=0.0
            temp = -1.0
            pause_mark = ONE_STEP
            #random.seed(iter*NUMBER_OF_SAMPLES) # moznost nastavit seed, hodilo by se vhodne nastavovat pri pripadne paralelizaci vypoctu
            c_state = 0
            while i < TIME:
                if temp == -1.0:
                    if not lambda_vector[c_state] == 0.0:
                        temp = random.expovariate(lambda_vector[c_state])
                        #print "iter" ,iter, "temp", temp
                    else: # jsme jiz ve stavu, z ktereho neni mozny prechod nikam jinam dal
                        while pause_mark <= TIME:
                            results[pause_mark].append(0.0)
                            pause_mark += ONE_STEP
                        break
                
                # print "i", i, "temp", temp, "pause_mark", pause_mark
                if (i+temp) >= pause_mark:
                    time_to_pause = pause_mark - i
                    if c_state in all_correctness:
                        results[pause_mark].append(1.0)
                    else:
                        results[pause_mark].append(0.0)
                    
                    i += time_to_pause
                    pause_mark += ONE_STEP
                    temp -= time_to_pause
                else:
                    # prejdeme do jineho stavu dle vygenerove nahodne promenne
                    random_val = 0.0
                    # je treba vyloucit nulu
                    while random_val == 0.0:
                        random_val = random.random()
                    
                    sum_of_prob = 0.0
                    for i_state, state in enumerate(matrix[c_state]):
                        sum_of_prob += state
                        if lambda_vector[c_state] == 0.0: # z tohoto stavu neni jiz prechod nikam jinam
                            break
                        if random_val < (sum_of_prob/lambda_vector[c_state]):
                            c_state = i_state
                            break
                    i += temp
                    temp = -1.0
    
        out_file = output.Output(str(OUTPUT))
        for key in sorted(results.keys()):
            out_file.write_line( str(key) + " " + str(numpy.mean(results[key])) )
            #print key, results[key]
        out_file.close()
    
    # Numericka metoda    
    elif METHOD == "numeric":
        # dopocteme prvyk na hlavni diagonale (pravdepodobnost setrvani v aktualni stavu)
        matrix = normalize_matrix(matrix, order_n)
        # jelikoz se zmenila matice, prekreslim i prechodovy graf
        dot_graph.draw(visited, matrix, all_correctness)
        
        # nastavime vektor vysledku
        vector = numpy.zeros((1, order_n))
        # budeme zacinat z pocatecniho plne funkcniho stavu
        vector[0][0] = 1.0
        
        out_file = output.Output(str(OUTPUT))
        out_file.write_line("0 1.0")
        
        i = 0
        pause_mark = 0
        while i < TIME:
            pause_mark += ONE_STEP
            while i < pause_mark:
                vector = numpy.dot(vector, matrix)
                i += 1
            print "Progress:", float(pause_mark)/float(TIME) * 100, "%"
            out_file.write_line(str(i)+" "+str(calculate_result(all_correctness, vector)))
    
        out_file.close()
    
    else:
        print "Unknown method."


