#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: parallel_ssa.py
# Popis: Soubor obsahuje skript pro spuštění paralelního výpočtu metody ssa
#        Podle zadaného počtu spustí N vláken
#        Z následných výsledků pak vypočte průměr a tedy finální výsledek
# Parametry: input.xml - vstupni soubor s definici systemu
#            output - nazev pro vystupni soubory
#            iterations - je zadáván pouze při použití metody ssa
#            threads - počet paralelních vláken (doporučuje se volit stejný jako počet jader v procesoru)
# Požadavky: Nainstalovanou aplikace 'graphviz' pro generování grafu
#            Nainstalován balíček 'NumPy'
# Použití: reliability_model.py input.xml output iterations threads

# mnou definovane knihovny
import output

# vestavene a ostatni knihovny
import os         # os.system
import sys        # sys.argv
import time       # sleep
from multiprocessing import Process
import subprocess # shell command
import numpy      # mean

# Funkce, ktera je spoustena paralelne
def run_command(number, input, output, iter):
    output = str(output)+str(number)
    #command = "reliability_model.py "+str(input)+" "+str(output)+" ssa "+str(iter)
    #os.system(command)
    cmd_array = ["python", "reliability_model.py", str(input), str(output), "ssa", str(iter)]
    #print cmd_array
    # pouze jeden soubor nam ukazuje orientacni prubeh vypoctu
    if number == 0:
        subprocess.call(cmd_array)
    else:
        subprocess.check_output(cmd_array)
    pass
    

#######################
# Hlavni funkce
#######################
if __name__ == "__main__":
    # kontrola zadanych parametru
    try:
        if len(sys.argv) < 5:
            raise

        INPUT = str(sys.argv[1])
        OUTPUT = str(sys.argv[2])
        ITERATIONS = int(sys.argv[3])
        THREADS = int(sys.argv[4])
        
    except:
        print "Wrong argument."
        print "USAGE: ./paralel_ssa.py input_file.xml output_file iterations threads)"
        exit(1)

    iter_per_thread = int(ITERATIONS/THREADS)
    
    processes = dict()
    
    # spustime pozadovany pocet procesu
    for i in range(THREADS):
        processes[i] = Process(target=run_command, args=(i, INPUT, OUTPUT, iter_per_thread))
        processes[i].start()    
    
    # pockame na jejich dokonceni
    for i in processes.keys():
        processes[i].join()
        
    # akumulujeme vysledky
    results = dict()
    
    # akumulace vysledku
    for i in range(THREADS):
        file_name = OUTPUT+str(i)
        
        lines = [line.strip() for line in open(file_name)]
        
        for line in lines:
            item = line.split(" ")
            time = int(item[0])
            value = float(item[1])
            if i == 0:
                results[time] = [value]
            else:
                results[time].append(value)
       
    # Vypocteni prumeru z vysledku a jejich zapis do vysledneho souboru
    out_file = output.Output(str(OUTPUT))
    for key in sorted(results.keys()):
        out_file.write_line( str(key) + " " + str(numpy.mean(results[key])) )
        #print key, results[key]
    out_file.close()



