#!/usr/bin/python
# coding=UTF8

# Autor: Jan Trávníček, xtravn09@stud.fit.vutbr.cz
# Soubor: matrix.py
# Popis: Soubor obsahuje definici pomocných funkcí pro generování matice přechodů a další pomocné funkce
# Požadavky: Nainstalovaný balíček 'NumPy'

import numpy
    
# funkce nam generuje mozne dalsi stavy v zavilosti na jednotkach, ktere se jeste mohou porouchat
# rozbiji pouze ty jednotky, ktere nejsou imunni
def damage_state(state, imunity):
    result = []
    for key in state.keys():
        newstate = state.copy()
        if ( state[key] > 0 ): # jestli se jeste muze neco rozbit, zkusime to rozbit
            if key in imunity.keys(): # je jednotka v seznamu nerozbitnych?
                if imunity[key] >= state[key]: # kontrola poctu nerozbitnych jednotek
                    continue # jednotku nerozbijej
            newstate[key] = newstate[key] - 1 # jinak jednotku rozbijeme
        result.append(newstate)
    return result

# funkce pro prehledny vypis matice
# UNUSED
def print_matrix(matrix):
    for i, row in enumerate(matrix):
        print i,
        for col in row:
            #print repr(col).rjust(6),
            print("%.8f" % col),
        print
        
# funkce overi, zda stav 'state' odpovida korektnimu stavu
def state_is_correct(state, array_correct):
    for correct_state in array_correct:
        is_correct = True
        for key in correct_state.keys():
            if state[key] < correct_state[key]:
                is_correct = False
        if is_correct:
            return True
    return False

# funkce generuje z pocatecniho nerozbiteho stavu vsechny mozne stavy
# funkce tedy vraci seznam, ktery reprezentuje mozny stavovy prostor s ohledem na imunitu (nerozbitnost) jednotek
def generate_statespace(data):
    start_state = data.count.copy()
    
    states_queue = [start_state] # zacneme se startovacim stavem    
    visited=[] # pomocne pole jiz vygenerovanych stavu
    
    while len(states_queue) > 0:
        current_state = states_queue.pop()
        new_states = damage_state(current_state, data.imunity)
        visited.append(current_state)
        for st in new_states:
            if not st in visited:
                states_queue.append(st)
    return visited

# funkce vraci seznam indexu vsech stavu, ktere jsou funkcni (nejsou poruchove)
# odpovidaji tedy pravidlum definovanym v sekci 'correctness'
def do_all_correctnes(visited, correctness):
    all_correctness = []
    for state in visited:
        if state_is_correct(state, correctness):
            try:
                index = visited.index(state)
                all_correctness.append(index)
            except:
                pass
    return all_correctness

# funkce overi, zda aktualni stav 'state' obsahuje potrebne jednotky pro opravu
# vraci odpovidajici hodnotu typu boolean
def is_repairable(state, must_working):
    repairable = True
    for key in must_working.keys():
        if state[key] < must_working[key]:
            repairable = False
#    print state, repairable
    return repairable

# funkce z predaneho vektoru 'vector' vypocte pravdepodobnost bezporuchoveho provozu R(t) pro vsechny korektni stavy
def calculate_result(all_correctness, vector):
    final_result = 0.0
    for index in all_correctness:
        final_result += vector[0][index]
    return final_result

# funkce se pouziva pro testovani, zda ma smysl pokracovat ve vypoctu dale
# funkce vraci True, pokud chceme vypocet ukoncit
# jinak vraci hodnotu False
def terminate_computation(new_vector, vector):
    # otestujeme, jestli se nektery radek matice rovna radku z minuleho kroku
    for i_row, row in enumerate(new_vector):
        if row == vector[i_row]:
            return True
        # otestujeme, jestli se nektery vysledek jiz nerovna jednicce
        if row[0] >= 1.0:
            return True
    return False
  
# funkce vygeneruje slovnik jednotek, ktere muzeme ziskat opravou nejake jednotky
# jako indexace je zvolen nazev jednotky, jako hodnota index stavu, do ktereho se prejde pripadnou opravou
def make_repair_list(state, visited):
    result = dict()
    temp_state = state.copy()
    for name in temp_state.keys(): # zkousime postupne rozbijet vsechny soucastky
        temp_state[name] = temp_state[name] + 1 # zkusime opravit
        try:
            index = visited.index(temp_state)
            result[name] = index
        except:
            index = -1 # aktualni soucastky tohoto typu jsou vsechny v poradku
        
        temp_state[name] = temp_state[name] - 1 # vratime do puvodniho stavu

    return result

# funkce vrati nazev jednotky, ktera se zmenila mezi dvemi danymi stavy
# funkce vraci pouze prvni vyskyt zmeny, ktery objevi
def what_change(start_state, end_state):
    for name in start_state.keys():
        if not start_state[name] == end_state[name]:
            return name
    return None

# funkce dopocitava pravdepodobnost na hlavni diagonale prechodove matice
# hodnoty na hlavni diagonale udavaji pravdepodobnost, ze zustave v atkualnim stavu
def normalize_matrix(matrix, order_n):
    for i in range(0, order_n):
        result = 1.0
        for st in range(0, order_n):
            result -= matrix[i][st]
        matrix[i][i] = result
    return matrix

# funkce dopocitava hodnotu na hlavni diagonale matice prechodu
# hodnoty na hlavni diagonale jsou soucty ostatnich prvku radku
# tento soucet je hodnota lambda pro setrvani v aktualnim stavu s exponencialnim rozlozenim
def make_lambda_vector(matrix, order_n):
    lambda_vector = []
    for i in range(0, order_n):
        result = 0.0
        for st in range(0, order_n):
            if not st == i:
                result += matrix[i][st]
        lambda_vector.append(result)
    return lambda_vector

def make_matrix_random_repair(visited, order_n, data):
    # vyrobime matici daneho radu a naplnime nulami
    matrix = numpy.zeros((order_n, order_n))
    
    for num_state, state in enumerate(visited): # pres vsechny mozne stavy
        temp_state = state.copy()
        repairable = is_repairable(temp_state, data.repair_working) # otestujeme, zda je stav opravitelny
        #print num_state, temp_state, repairable
        for name in temp_state.keys(): # zkousime postupne rozbijet nebo opravovat vsechny soucastky
            if repairable: # je stav opravitelny?
                temp_state[name] = temp_state[name] + 1 # zkusime opravit
                try:
                    index = visited.index(temp_state)
                    matrix[num_state][index] = data.mu[name]
                except:
                    pass # aktualni soucastky tohoto typu jsou vsechny v poradku
                temp_state[name] = temp_state[name] - 1 # vratime do puvodniho stavu
            # endif: je stav opravitelny?
            temp_state[name] = temp_state[name] - 1 # zkusime rozbit
            try:
                index = visited.index(temp_state)
                matrix[num_state][index] = data.lam[name]
            except:
                pass # aktualni soucastka se nemuze uz polamat, vsechny uz jsou polamany
            temp_state[name] = temp_state[name] + 1
    return matrix

def make_matrix(visited, order_n, data):
    # vyrobime matici daneho radu a naplnime nulami
    matrix = numpy.zeros((order_n, order_n))
    
    # generujeme matici prechodu
    for num_state, state in enumerate(visited): # pres vsechny mozne stavy
        temp_state = state.copy()
        #print num_state, temp_state
        # nejdriv jednotky budeme rozbijet
        for name in temp_state.keys(): # zkousime postupne rozbijet vsechny soucastky
            temp_state[name] = temp_state[name] - 1 # zkusime rozbit
            try:
                index = visited.index(temp_state)
                matrix[num_state][index] = data.lam[name]
            except:
                pass # aktualni soucastka se nemuze uz polamat, vsechno uz je porouchane
            temp_state[name] = temp_state[name] + 1
        
        # pak je budeme opravovat podle pravidel pro opravu
        if is_repairable(temp_state, data.repair_working): # Mame potrebne jednotky pro opravu?
            repair_list = make_repair_list(temp_state, visited) # mozne stavy, do kterych muzeme prejit opravou
            repair_rule = None
            if repair_list: # jestli mame vubec co opravovat
                # hledame pravidlo pro opravu
                for item in data.repair_rules: # hledame odpovidajici pravidlo podle priorit sestupne
                    status = True
                    for component in item[1].keys():
                        if not item[1][component] == temp_state[component]:
                            status = False
                            break
                    if status:
                        repair_rule = item
                        break
                #print "Vybrane pravidlo opravy", repair_rule
                index_of_repair = -1
                for item in repair_rule[2]:
                    try:
                        index_of_repair = repair_list[item[0]]
                        what_is_repair = item[0]
                        break
                    except:
                        pass
                if index_of_repair == -1:
                    print "Nastala neocekavana chyba pri generovani matice v sekci opravy."
                    exit()
            
                matrix[num_state][index_of_repair] = data.mu[what_is_repair]
                
    return matrix


